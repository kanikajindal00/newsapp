﻿
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

public class Sentiments
{
    public Result result { get; set; }
    public List<Sentence> sentences { get; set; }
}
public class SentimModel
{
    public string text { get; set; }
}

public class Result
{
    public string author { get; set; }
    public string title { get; set; }

    public float polarity { get; set; }
    public string category { get; set; }

}

public class Sentence
{
    public string sentence { get; set; }
    public Sentiment sentiment { get; set; }
}

public class Sentiment
{
    public float polarity { get; set; }
    public string type { get; set; }
}
