﻿namespace NewsApp.Models
{
    public class NewsItems
    {
        public string category { get; set; }
        public NewsObject[] data { get; set; }
        public bool success { get; set; }
    }



    public class NewsObject
    {
        public string author { get; set; }
        public string title { get; set; }
    }
}
