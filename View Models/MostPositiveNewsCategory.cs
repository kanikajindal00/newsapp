﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsApp.View_Models
{
    public class MostPositiveNewsCategory
    {
        public string Category { get; set; }
        public float AvgSentimentPolarity { get; set; }
    }
}
