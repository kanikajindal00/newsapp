﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsApp.View_Models
{
    public class MostPositiveNewsAuthor
    {
        
            public string Author { get; set; }
            public float AvgSentimentPolarity { get; set; }
        
    }
    public class NewsAuthor
    {

        public string Author { get; set; }
        public string title { get; set; }
        public float AverageSentimentPolarity { get; set; }

    }
}
