﻿using Microsoft.AspNetCore.Mvc;
using NewsApp.Models;
using NewsApp.Services;
using NewsApp.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace NewsApp.APIControllers
{

    public class NewsController : BaseAPIController
    {
        private readonly IInShortsApiService _inShortsService;
        /*String[] category = new String[12]{ "national", "business","sports", "world", "politics",
                "technology","startup", "entertainment", "miscellaneous", " hatke","science", "automobile" };*/
        String[] category = new String[2] { "national", "business" };
        public NewsController(IInShortsApiService inShortsService)
        {
            _inShortsService = inShortsService;
           
        }

        [HttpGet]
        [Route("getMostPositiveNewsCategory")]
        public async Task<MostPositiveNewsCategory> GetMostPositiveNewsCategory()
        {
           
            MostPositiveNewsCategory result = new MostPositiveNewsCategory();
            result = await _inShortsService.GetNewsItemsOnCategory(category);
            return result;
        }

        [HttpGet]
        [Route("getMostPositiveNewsAuthor")]
        public async Task<MostPositiveNewsAuthor> GetMostPositiveNewsAuthor()
        {

            MostPositiveNewsAuthor result = new MostPositiveNewsAuthor();
            result = await _inShortsService.GetNewsItemsOnAuthor(category);
            return result;
        }

        [HttpGet]
        [Route("getTop3PositiveArticles")]
        public async Task<List<TopPositiveArticles>> GetTop3PositiveArticles()
        {
            
            List<TopPositiveArticles> result = new List<TopPositiveArticles>();
            result = await _inShortsService.GetTop3PositiveArticles(category);
            return result;
        }


    }
}
