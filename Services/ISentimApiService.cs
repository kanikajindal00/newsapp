﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsApp.Services
{
    public interface ISentimApiService
    {
        public Task<List<Result>> getMostPositiveNewsCategory(List<NewsObject> items, string category);
        public Task<List<Result>> getMostPositiveNewsAuthorAndTop3News(List<NewsObject> items,string category);
        
    }
}
