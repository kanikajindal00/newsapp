﻿using NewsApp.Models;
using NewsApp.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace NewsApp.Services.Implementations
{
    public class InShortsApiService : IInShortsApiService
    {
        private readonly HttpClient _httpClient;
        private readonly ISentimApiService _sentimApiService;

        public InShortsApiService(ISentimApiService sentimApiService)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri("https://inshortsapi.vercel.app/news")
            };
            _sentimApiService = sentimApiService;
        }



        public async Task<MostPositiveNewsCategory> GetNewsItemsOnCategory(string[] category)
        {
            var result = new MostPositiveNewsCategory();
            var query = new List<MostPositiveNewsCategory>();
            List<Result> list = new List<Result>();

            var newsItems = new NewsItems();
            for (int i = 0; i < category.Length; i++)
            {
                var url = string.Concat("?category=", category[i]);
                var response = await _httpClient.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var stringResponse = await response.Content.ReadAsStringAsync();

                    newsItems = JsonSerializer.Deserialize<NewsItems>(stringResponse,
                        new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
                }
                else
                {
                    throw new HttpRequestException(response.ReasonPhrase);
                }
                var allSentiments = await _sentimApiService.getMostPositiveNewsCategory(newsItems.data.ToList(), category[i]);
                list.AddRange(allSentiments);

            }
            query = list.GroupBy(t => new { title = t.title,category = t.category })
                   .Select(g => new MostPositiveNewsCategory()
                   {
                       Category = g.Key.category,
                       AvgSentimentPolarity = g.Max(p => p.polarity)
                   }).ToList();
            var maxValue = list.Max(x => x.polarity);
            result = query.Where(x => x.AvgSentimentPolarity == maxValue).Select(g => new MostPositiveNewsCategory()
            {
                Category = g.Category,
                AvgSentimentPolarity = g.AvgSentimentPolarity
            }).FirstOrDefault();
            return result;
        }

        public async Task<MostPositiveNewsAuthor> GetNewsItemsOnAuthor(string[] category)
        {
            var result = new MostPositiveNewsAuthor();
            var list = await commonUtilForcallingSentimAPI(category);
            var query = list.GroupBy(t => new { Author = t.author })
                       .Select(g => new NewsAuthor()
                       {
                           Author = g.Key.Author,
                           AverageSentimentPolarity = g.Average(p => p.polarity)
                       });
            result = query.Select(g => new  MostPositiveNewsAuthor()
            {
                Author = g.Author,
                AvgSentimentPolarity = g.AverageSentimentPolarity
            }).OrderByDescending(x => x.AvgSentimentPolarity).FirstOrDefault();
            return result;
        }



        public async Task<List<TopPositiveArticles>> GetTop3PositiveArticles(string[] category)
        {
            var result = new List<TopPositiveArticles>();
            var list = await commonUtilForcallingSentimAPI(category);
            result = list.Select(g => new TopPositiveArticles()
            {
                author = g.author,
                category = g.category,
                title = g.title,
                sentimentPolarity = g.polarity
            }).OrderByDescending(x => x.sentimentPolarity).Take(3).ToList();
          
            return result;
        }

        public async Task<List<Result>> commonUtilForcallingSentimAPI(string[] category)
        {
            List<Result> list = new List<Result>();

            var newsItems = new NewsItems();
            for (int i = 0; i < category.Length; i++)
            {
                var url = string.Concat("?category=", category[i]);
                var response = await _httpClient.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var stringResponse = await response.Content.ReadAsStringAsync();

                    newsItems = JsonSerializer.Deserialize<NewsItems>(stringResponse,
                        new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
                }
                else
                {
                    throw new HttpRequestException(response.ReasonPhrase);
                }
                var allSentiments = await _sentimApiService.getMostPositiveNewsAuthorAndTop3News(newsItems.data.ToList(), category[i]);
                list.AddRange(allSentiments);

            }
            return list;
        }
    }
}
