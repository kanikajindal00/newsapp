﻿using NewsApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace NewsApp.Services.Implementations
{
    public class SentimApiService : ISentimApiService
    {
        private readonly HttpClient _httpClient;
        public SentimApiService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://sentim-api.herokuapp.com/api/v1/");
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Result>> getMostPositiveNewsCategory(List<NewsObject> items,string category)
        {
            var result = new List<Result>();
            var allTitle = "";
            foreach (var title in items)
            {
                allTitle += title.title +". " + Environment.NewLine;
            }
            var model = new SentimModel()
            {
                text = allTitle
            };
            var json = JsonConvert.SerializeObject(model);
            
            HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("", httpContent);

            if (response.IsSuccessStatusCode)
            {

                var stringResponse = await response.Content.ReadAsStringAsync();
                var temp = System.Text.Json.JsonSerializer.Deserialize<Sentiments>(stringResponse,
                       new JsonSerializerOptions()
                       {
                           PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                       });
                var res = new Result()
                {
                    polarity = temp.result.polarity,
                    category = category
                };
                result.Add(res);
                
            }
            else
            {
                throw new HttpRequestException(response.ReasonPhrase);
            }
          
            
            /*result.GroupBy(x => x.sentences).Average(y => y.Select(z => z.result.polarity));*/
            return result;
        }

        public async Task<List<Result>> getMostPositiveNewsAuthorAndTop3News(List<NewsObject> items,string category)
        {
            var res = new Result();
            var result = new List<Result>();
            foreach (var item in items)
            {
                var model = new SentimModel()
                {
                    text = item.title
                };
                
                var json = JsonConvert.SerializeObject(model);

                HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync("", httpContent);

                if (response.IsSuccessStatusCode)
                {

                    var stringResponse = await response.Content.ReadAsStringAsync();
                    var temp = System.Text.Json.JsonSerializer.Deserialize<Sentiments>(stringResponse,
                        new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                        });
                    res = new Result()
                    {
                        polarity = temp.result.polarity,
                        author = item.author,
                        title = item.title,
                        category = category
                    };
                    result.Add(res);
                    
                }
                else
                {
                    throw new HttpRequestException(response.ReasonPhrase);
                }

            }
            return result;
        }
      

    }
}
