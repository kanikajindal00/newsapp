﻿using NewsApp.Models;
using NewsApp.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsApp.Services
{
    public interface IInShortsApiService
    {
        public Task<MostPositiveNewsCategory> GetNewsItemsOnCategory(string[] category);
        public Task<MostPositiveNewsAuthor> GetNewsItemsOnAuthor(string[] category);
        public Task<List<TopPositiveArticles>> GetTop3PositiveArticles(string[] category);
        public Task<List<Result>> commonUtilForcallingSentimAPI(string[] category);
    }
}
